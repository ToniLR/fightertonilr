﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hurtbox : MonoBehaviour
{

    public GameObject enemigo;
    public Animator animator;
    public GameObject sangre;
    private Vector2 moveInput;

  

    // Start is called before the first frame update
    void Start()
    {
       

    }

    // Update is called once per frame
    void Update()
    {
      
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {

        //Si no esta bloqueando comprueba que colisione con sus hitboxs
        if (!enemigo.GetComponent<EnemigoCombate>().bloquear)
        {
            //Le inflinge daño y le crea un knockback y cambia a la animacion de golpeado , y instancia sangre
            if (collision.gameObject.name == "Puño")
            {
      
               // print("TOCADO");

                enemigo.GetComponent<Rigidbody2D>().AddForce(new Vector2(100, 100));
                enemigo.gameObject.GetComponent<EnemigoPropiedades>().HP -= 5;
                Instantiate(sangre, this.transform.position, Quaternion.identity);
                animator.SetTrigger("golpeado");

                // collision.gameObject.GetComponent<EnemigoPropiedades>().HP -= dañoAtaque;
            }
            if (collision.gameObject.name == "Puño2")
            {

                enemigo.GetComponent<Rigidbody2D>().AddForce(new Vector2(100, 100));
                enemigo.gameObject.GetComponent<EnemigoPropiedades>().HP -= 10;
               // print("TOCADO");
                animator.SetTrigger("golpeado");
                // collision.gameObject.GetComponent<EnemigoPropiedades>().HP -= dañoAtaque;
            }
            if (collision.gameObject.name == "Pierna")
            {
           
                enemigo.GetComponent<Rigidbody2D>().AddForce(new Vector2(100, 100));
                enemigo.gameObject.GetComponent<EnemigoPropiedades>().HP -= 10;
               // print("TOCADO PIERNA");
                animator.SetTrigger("golpeado");
                // collision.gameObject.GetComponent<EnemigoPropiedades>().HP -= dañoAtaque;
            }
            if (collision.gameObject.name == "Pierna2")
            {
           
                enemigo.GetComponent<Rigidbody2D>().AddForce(new Vector2(100, 100));
                enemigo.gameObject.GetComponent<EnemigoPropiedades>().HP -= 20;
                //print("TOCADO");
                animator.SetTrigger("golpeado");
                // collision.gameObject.GetComponent<EnemigoPropiedades>().HP -= dañoAtaque;
            }
            if (collision.gameObject.tag == "proyectil")
            {

                //print("ENEMIGO COLISIONA CON PROYECTIL");
                GameObject.Destroy(collision.gameObject);
                animator.SetTrigger("golpeado");
                enemigo.GetComponent<Rigidbody2D>().AddForce(new Vector2(300, 200));
                enemigo.gameObject.GetComponent<EnemigoPropiedades>().HP -= 40;
            }
        }
    }
   

}
