﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JinCombate : MonoBehaviour
{
    public Animator animator;
    public GameObject proyectil;
    public Transform puño;
    public Transform puño2;
    public Transform pierna;
    public Transform pierna2;

    public float rangoAtaque = 0.5f;
    public LayerMask layersEnemigos;
    int combostatus = 0;
    int cont = 0;
    public int dañoAtaque = 5;
    public bool bloquear = false;
  
    public int bonus = 0;
    // Update is called once per frame
    void Update()
    {
        

        bloquear = false;
        //print(combostatus);

        //Proyectil solo cuando bonus es 5 o mas , SE carga con golpes
        if (Input.GetKeyDown("q") && bonus>=5)
        {
            animator.SetTrigger("proyectil");
            disparar();
            bonus = 0;

        }
        //puño y set de movimientos 
        if (Input.GetKeyDown("w"))
        {
          
                bonus++;
            
          
            if (combostatus == 2)
            {
                animator.SetTrigger("PegarFuerte");
              
            }
            else if (combostatus == 3)
            {
                animator.SetTrigger("PatadaReves");
               
            }
            else
            {
                animator.SetTrigger("Pegar");
              
            }
   
            StartCoroutine(Combos("w"));
        }
        
        //combo patada
        if (Input.GetKeyDown("e"))
        {

            if (combostatus == 1)
            {
                animator.SetTrigger("patada");
              
            }

           
            StartCoroutine(Combos("e"));
        }
        //block
        if (Input.GetKey("r"))
        {

            animator.SetTrigger("block");
            bloquear = true;

        }
       //combo W+SPACE patada voladora animacion
        if (Input.GetKeyDown("space"))
        {

            StartCoroutine(Combos("space"));
            

        }
       
    }

    //Disparo de proyectil
    private void disparar()
    {
        GameObject aux = (GameObject)Instantiate(proyectil);
        aux.transform.position = this.transform.position;
    }


    //CORRUTINA COMBOS
    IEnumerator Combos(String key)
    {
        //Set de animaciones con la w hasta 4
        if (key == "w")
        {
           // print(cont + "contador pa");
            cont++;
            if (combostatus == 0)
            {
                combostatus = 1;
                yield return new WaitForSeconds(1f);
                if (combostatus == 1)
                {
                    combostatus = 0;
                   // print("Combo fallido");

                    cont = 0;
                    yield return 0;
                }
            }
            if (combostatus == 1 && cont == 2)
            {
                //print("COMBO1");
                combostatus = 2;
                
                yield return new WaitForSeconds(1f);
                if (combostatus == 2)
                {
                    combostatus = 0;
                  //  print("Combo fallido");
                    cont = 0;
                    yield return 0;
                }

            }
            if (combostatus == 2 && cont==3)
            {
                //print("COMBO4");
                
            
                combostatus = 3;
                yield return new WaitForSeconds(1f);
                if (combostatus == 3)
                {
                    cont = 0;
                    combostatus = 0;
                    //print("COMBO1 FALLADO");
                    yield return 0;
                }

            }

 
        }

        //Combo W+E 
        else if (key == "e")
        {
           
            if (combostatus == 1)
            {

                    bonus++;
                
                //print("Combo2");
                combostatus = 4;
                yield return new WaitForSeconds(1f);
                if (combostatus == 4)
                {
                    combostatus = 0;
                   // print("Combo fallido");
                   
                    yield return 0;
                 
                }

            }
            else
            {
                combostatus = 0;
                //print("tecla fuera de tiempo");
            }
            

        //Combo w+space
        }else if (key=="space")
        {
            if (combostatus == 1)
            {
                //print("Combo3");
                combostatus = 5;
              
                    bonus++;
                
                animator.SetTrigger("AtaqueAereo");
                yield return new WaitForSeconds(1.5f);
                if (combostatus == 5)
                {
                    combostatus = 0;
                   // print("Combo fallido");
               
                    yield return 0;
                  
                }
            }

            if (combostatus == 5)
            {

                combostatus = 0;
            }
        } else
        {
            combostatus = 0;
        }


        cont = 0;
    }

}
