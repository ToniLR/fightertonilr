﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuJugadorController : MonoBehaviour
{

    public static bool jin;

    /*
    * Funcion que sirve para el menu de Seleccion de jugador
    * Carga el juego y activa el boolean jin que significa que eres jin
    * 
    * */
    public void CargarJuegoJin()
    {
        jin = true;
        SceneManager.LoadScene(MenuMapasController.Escena);

    }
    /*
    * Funcion que carga el juego como Sagat
    */
    public void CargarJuegoSagat()
    {
        jin = false;
        SceneManager.LoadScene(MenuMapasController.Escena);

    }

}
