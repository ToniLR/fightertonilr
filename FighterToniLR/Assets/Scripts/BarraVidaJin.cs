﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BarraVidaJin : MonoBehaviour
{

    private Image Barra;

    private float CurrentHP;
    public float MaxHP = 100f;
    JinController jin;

    // Start is called before the first frame update
    void Start()
    {
        Barra = GetComponent<Image>();
        jin = FindObjectOfType<JinController>();
    }
    /**Igualas currentHp a la del personaje
    * y lo divides entre la maxima para del
    * 0 al 1 mover el fillAmount de la imagen
    */
    // Update is called once per frame
    void Update()
    {
        CurrentHP = jin.HP;
        Barra.fillAmount = CurrentHP / MaxHP;

    }
}
