﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class JinController : MonoBehaviour
{


    bool salto = false;
    public  int HP = 100;
    public Animator animacion;
    public GameObject enemigo;
    public GameObject[] carteles;
    public Text Texto;
    
    int cont;

    // Start is called before the first frame update
    void Start()
    {
      
    }

    // Update is called once per frame
    void Update()
    {
        if (HP > 100)
        {
            HP = 100;
        }

        inputs();

        /**Si muere el personaje se declara el text que dice que con la M vuelves al menu  
        * Miras si eres Jin para saber si en este caso has perdido o no  y elimina al personaje 
        */
        if (HP <= 0)
        {
      
            Texto.text += "\nPulsa M para volver al menu";
            if (Input.GetKeyDown("m"))
            {
                SceneManager.LoadScene(0);
            }
            if (MenuJugadorController.jin)
            {

                Instantiate(carteles[1]);
            }
            else
            {
                Instantiate(carteles[0]);
            }

            this.gameObject.transform.position = new Vector2(18, -1);
            // GameObject.Destroy(this.gameObject);
        }
        
        ComprobarResultado();

    }
    
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "suelo")
        {
            //boolean para evitar el salto infinito
            salto = true;

        }
        //item que da vida , esta en el mapa 1 , solo hay 1 durante toda la partida , solo suma si tienes menos de la MaxHP
        if (collision.gameObject.tag == "vida")
        {
            if (HP < 100)
            {
                HP += 30;
                GameObject.Destroy(GameObject.FindWithTag("vida"));
            }
        }
        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        // si estas en la piscina de lava te quita vida y hay doble salto para poder salir 
        if (collision.gameObject.tag=="Fuego")
        {
        
            if (cont >= 2)
            {
                salto = true;
                cont = 0;
            }
            
                HP -= 5;

            
           
        }       
    }


    void inputs()
    {

        // te mueves hacia delante y desactivas el boolean de ir para atras del animator
        if (Input.GetKey("d"))
        {

            this.GetComponent<Rigidbody2D>().velocity = new Vector2(3.5f, this.GetComponent<Rigidbody2D>().velocity.y);
            animacion.SetBool("Correr", true);
            animacion.SetBool("ParaAtras", false);
        }
        // te mueves para atras y desactivas el boolean de ir para delante del animator
        if (Input.GetKey("a"))
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(-3.5f, this.GetComponent<Rigidbody2D>().velocity.y);
            animacion.SetBool("ParaAtras", true);
            animacion.SetBool("Correr", false);
        }


        // el salto
        if (Input.GetKeyDown("space") && salto)
        {
            cont++;
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 300));
            animacion.SetBool("Salto", true);
            salto = false;
        }
        if (!salto)
        {
            animacion.SetBool("Salto", false);
        }
        if (this.GetComponent<Rigidbody2D>().velocity.x == 0)
        {
            animacion.SetBool("Correr", false);
            animacion.SetBool("ParaAtras", false);

        }
       
    }

    public void ComprobarResultado()
    {

       if(enemigo.GetComponent<EnemigoPropiedades>().HP <= 0)
        {

           animacion.SetTrigger("Win");

        }

    }
}
