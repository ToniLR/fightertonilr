﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuMapasController : MonoBehaviour
{

     public static int Escena;

    //Funcion para cargar el mapa 1 , se usa en el boton 1 del menu 
    public void GetScene1()
    {
        Escena = 1;
    }

    //Funcion para cargar el mapa 2 , se usa en el boton 2 del menu 
    public void GetScene2()
    {
        Escena = 2;
    }
    //Funcion para cargar el mapa 3 , se usa en el boton 3 del menu 
    public void GetScene3()
    {
        Escena = 3;
    }




}
