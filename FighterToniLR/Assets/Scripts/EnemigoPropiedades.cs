﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class EnemigoPropiedades : MonoBehaviour
{

    public  int HP = 100;
    public GameObject[] carteles;
    public Text Texto;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (HP > 100)
        {
            HP = 100;
        }

        /**Si muere el personaje se declara el text que dice que con la M vuelves al menu  
       * Miras si eres Jin para saber si en este caso has perdido o no  y elimina al personaje 
       */
        if (HP <= 0)
        {   
            Texto.text += "\nPulsa M para volver al menu";

            if (Input.GetKeyDown("m"))
            {
                SceneManager.LoadScene(0);
            }
            if (MenuJugadorController.jin)
            {
                Instantiate(carteles[0]);
            }
            else
            {
                Instantiate(carteles[1]);
            }

            this.gameObject.transform.position =new Vector2(18,-1);
           // GameObject.Destroy(this.gameObject);
           
        }
    

        //activa letra para volver al menu del juego

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        //colision que el lago de lava del mapa 1 quita vida
        if (collision.gameObject.tag == "Fuego")
        {
        
                HP -= 5;

        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Colision con item que da vida en el mapa 1
        if (collision.gameObject.tag == "vida")
        {
           
                if (HP < 100)
                {
                    HP += 30;
                    GameObject.Destroy(GameObject.FindWithTag("vida"));
                }
            
        }


    }
}
