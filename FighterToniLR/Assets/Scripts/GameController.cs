﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{

    public float timeleft = 120f;
    public Text texto;
    public Text texto2;
    public GameObject[] carteles;
    public GameObject J;
    // Start is called before the first frame update
    void Start()
    {
        //J al lado del icono jugador para ver tu personaje elegido 
        if (MenuJugadorController.jin)
        {
            Instantiate(J,new Vector3(-7.19f, 3.28f, 0),Quaternion.identity);
          
        }
        else
        {
            Instantiate(J, new Vector3(7.19f, 3.28f, 0), Quaternion.identity);
        }
        
    }

    // Update is called once per frame
    void Update()
    {
       // Situacion de que se acaba el tiempo y nadie gane

        timeleft -= Time.deltaTime;
        texto.text = timeleft.ToString("0");

        if (timeleft < 0)
        {
             texto2.text += "\nPulsa M para volver al menu";
            timeleft = 0;
            GameObject.Destroy(GameObject.FindWithTag("hitbox"));
            GameObject.Destroy(GameObject.FindWithTag("enemigo"));
            Instantiate(carteles[1]);

            if (Input.GetKeyDown("m")){
                SceneManager.LoadScene(0);
            }

        }
    }
}
