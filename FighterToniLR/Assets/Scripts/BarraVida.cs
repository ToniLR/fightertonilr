﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BarraVida : MonoBehaviour
{

    private Image Barra;

    private float CurrentHP;
    public float MaxHP = 100f;
    EnemigoPropiedades sagat;

    // Start is called before the first frame update
    void Start()
    {
        Barra = GetComponent<Image>();
        sagat = FindObjectOfType<EnemigoPropiedades>();
    }
    /**Igualas currentHp a la del personaje
    * y lo divides entre la maxima para del
    * 0 al 1 mover el fillAmount de la imagen
    */
    // Update is called once per frame
    void Update()
    {
        CurrentHP = sagat.HP;
        Barra.fillAmount = CurrentHP / MaxHP;
        
    }
}
