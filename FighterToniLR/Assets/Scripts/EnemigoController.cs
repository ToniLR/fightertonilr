﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoController : MonoBehaviour
{

    public Animator animator;
    public GameObject jin;
 
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //Movimiento Sagat con flechas
        if (Input.GetKey("right")){
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(3.5f, this.GetComponent<Rigidbody2D>().velocity.y);
            animator.SetTrigger("correr");
        }
        if (Input.GetKey("left"))
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(-3.5f, this.GetComponent<Rigidbody2D>().velocity.y);
            animator.SetTrigger("correr");
        }



        comprobarResultado();

    }

    public void comprobarResultado(){

        if (jin.GetComponent<JinController>().HP <= 0)
        {

            animator.SetTrigger("win");

        }

    }

}
