﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BonusJin : MonoBehaviour
{
    //Script para la barra de Bonus de Jin
    //Bonus se carga dando golpes , con 5 cargas todo el bonus y puedes lanzar el proyectil

    private Image imagen;

    private float CurrentBonus;
    public float MaxBonus = 5f;
    JinCombate jin;
    // Start is called before the first frame update
    void Start()
    {
        imagen = GetComponent<Image>();
        jin = FindObjectOfType<JinCombate>();
    }
    /*Igualas el CurrentBonus al bonus de Sagat 
    * y divides el current bonus entre el Max
    * para del 0 al 1 cambie el FillAmount de la imagen Barra
    * */
    // Update is called once per frame
    void Update()
    {
        CurrentBonus = jin.bonus;
        imagen.fillAmount = CurrentBonus / MaxBonus;
    }
}
