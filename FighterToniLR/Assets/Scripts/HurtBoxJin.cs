﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HurtBoxJin : MonoBehaviour
{
    public GameObject jin;
    public GameObject sangre;
    public Animator animator;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {

        //compruebas que no esta bloqueando daño y comprueba parte del hitbox con la que lo daña
        if (!jin.GetComponent<JinCombate>().bloquear)
        {
            //le inflinges daño , le creas un knockback , animacion y instancias sangre 

            if (collision.gameObject.name == "puño")
            {
             
                jin.GetComponent<Rigidbody2D>().AddForce(new Vector2(-100, 100));
                //print("TOCADO");
                jin.gameObject.GetComponent<JinController>().HP -= 5;
                animator.SetTrigger("golpeado");
                Instantiate(sangre, this.transform.position, Quaternion.identity);
                // collision.gameObject.GetComponent<EnemigoPropiedades>().HP -= dañoAtaque;
            }
          
            if (collision.gameObject.name == "Pierna")
            {
       
                jin.GetComponent<Rigidbody2D>().AddForce(new Vector2(-100, 100));
                jin.gameObject.GetComponent<JinController>().HP -= 10;
                animator.SetTrigger("golpeado");
               // print("TOCADO PIERNA");
                // collision.gameObject.GetComponent<EnemigoPropiedades>().HP -= dañoAtaque;
            }
            if(collision.gameObject.tag == "proyectilS")
            {
                //print("JIN COLISIONA CON PROYECTIL");
                GameObject.Destroy(collision.gameObject);
                jin.GetComponent<Rigidbody2D>().AddForce(new Vector2(-300, 200));
                animator.SetTrigger("golpeado");
                jin.gameObject.GetComponent<JinController>().HP -= 30;
            }
          
        }
    
       
    }


}
