﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class proyectilSagat : MonoBehaviour
{
    public float speed = 6f;
    public int dañoAtaque = 20;

    //destruye el proyectil al alcanzar ese tiempo
    void Start()
    {
        GameObject.Destroy(this.gameObject, 1f);
    }

    // le da el movimiento al disparo
    void Update()
    {

        this.transform.Translate(Vector2.right * speed * Time.deltaTime);
        this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 100 * speed * Time.deltaTime));
    }

}
