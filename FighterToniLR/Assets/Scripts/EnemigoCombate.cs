﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemigoCombate : MonoBehaviour
{

    public Animator animator;
    public GameObject proyectil;
    bool salto = false;
   public   bool bloquear = false;
    int combostatus = 0;
    int cont;

    public int bonus = 0;
    // Start is called before the first frame update
    void Start()
    {
      
    }

    // Update is called once per frame
    void Update()
    {
        bloquear = false;
        //salto 
        if (Input.GetKey("down"))
        {
            cont++;
            if (salto)
            {
                this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 300));

                salto = false;
            }
           
            StartCoroutine(Combos("down"));
            //animator.SetTrigger("proyectil");
            // disparar();

        }
        //puño 
        if (Input.GetKeyDown("up"))
        {
            bonus++;
            animator.SetTrigger("puño");
            StartCoroutine(Combos("up"));
        }
        //block y que no este bloqueando ya 
        if (Input.GetKey("2") && bloquear==false)
        {

            animator.SetTrigger("block");
            bloquear = true;
        }
        if (Input.GetKey("1") && bonus>=5)
        {
            animator.SetTrigger("proyectil");
            disparar();
            bonus = 0;
        }
     
    }
    //disparo sagat
    private void disparar()
    {
        GameObject aux = (GameObject)Instantiate(proyectil);
        aux.transform.position = this.transform.position;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Colision con la piscina de lava del primer mapa 
        if (collision.gameObject.tag == "Fuego")
        {
          
            if (cont >= 2)
            {
                salto = true;
                cont = 0;
            }

        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Comprueba que estas en el suelo para poder saltar
        if (collision.gameObject.tag == "suelo")
        {
            salto = true;

        }
    }
   
    IEnumerator Combos(string key)
    {
        if(key == "up")
        {
            if (combostatus == 0)
            {

                combostatus = 1;
                yield return new WaitForSeconds(1.5f);
                if (combostatus == 1)
                {
                    combostatus = 0;
                    // print("Combo fallido");

                    yield return 0;
                }
            }

        }
        //Combo Flecha para arriba y flecha abajo
        if (key == "down")
        {

            if (combostatus == 1)
            {
                combostatus = 2;
                bonus++;
                animator.SetTrigger("patada");
                this.GetComponent<Rigidbody2D>().gravityScale = 0.75f;
                yield return new WaitForSeconds(1.5f);
                this.GetComponent<Rigidbody2D>().gravityScale =1f;
                if (combostatus == 2)
                {
                    combostatus = 0;
                    // print("Combo fallido");

                    yield return 0;
                }

            }
        }
        //this.GetComponent<Rigidbody2D>().gravityScale = 1f;
        yield return 0;
    }
}
    