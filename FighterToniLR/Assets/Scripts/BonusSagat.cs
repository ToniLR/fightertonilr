﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BonusSagat : MonoBehaviour
{
    //Script para la barra de Bonus de Sagat
    //Bonus se carga dando golpes , con 5 cargas todo el bonus y puedes lanzar el proyectil

    private Image Barra;

    EnemigoCombate Sagat;
    private float CurrentBonus;
    public float MaxBonus = 5f;

    // Start is called before the first frame update
    void Start()
    {
        Barra = GetComponent<Image>();
        Sagat = FindObjectOfType<EnemigoCombate>();

    }
    /*Igualas el CurrentBonus al bonus de Sagat 
    * y divides el current bonus entre el Max
    * para del 0 al 1 cambie el FillAmount de la imagen Barra
    * */
    // Update is called once per frame
    void Update()
    {
        CurrentBonus = Sagat.bonus;
        Barra.fillAmount = CurrentBonus / MaxBonus;
    }
}
